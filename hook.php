<?php
/*
 -------------------------------------------------------------------------
 wunderlist plugin for GLPI
 Copyright (C) 2017 by the wunderlist Development Team.

 https://bitbucket.org/staltrans/wunderlist
 -------------------------------------------------------------------------

 LICENSE

 This file is part of wunderlist.

 wunderlist is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 wunderlist is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with wunderlist. If not, see <http://www.gnu.org/licenses/>.
 --------------------------------------------------------------------------
 */

/**
 * Plugin install process
 *
 * @return boolean
 */
function plugin_wunderlist_install() {

   global $DB;

   $t_auths = 'glpi_plugin_wunderlist_auths';
   $t_roots = 'glpi_plugin_wunderlist_roots';
   $t_users = 'glpi_plugin_wunderlist_users';
   $t_lists = 'glpi_plugin_wunderlist_lists';
   $t_tasks = 'glpi_plugin_wunderlist_tasks';
   $t_settings = 'glpi_plugin_wunderlist_settings';

   if (!TableExists($t_auths)) {
      $query = "CREATE TABLE `$t_auths` (
                  `id` int(11) NOT NULL AUTO_INCREMENT,
                  `users_id` int(11) NOT NULL DEFAULT '0',
                  `date_mod` timestamp NULL DEFAULT NULL,
                  `date_creation` timestamp NULL DEFAULT NULL,
                  `state` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
                  `code` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
                  `access_token` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
                  PRIMARY KEY (`id`),
                  UNIQUE KEY (`users_id`),
                  KEY `date_mod` (`date_mod`),
                  KEY `date_creation` (`date_creation`),
                  KEY `state` (`date_creation`),
                  KEY `code` (`date_creation`),
                  KEY `access_token` (`access_token`)
                ) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci";
      $DB->queryOrDie($query);
   }

   if (!TableExists($t_roots)) {
      $query = "CREATE TABLE `$t_roots` (
                  `id` int(11) NOT NULL AUTO_INCREMENT,
                  `users_id` int(11) NOT NULL DEFAULT '0',
                  `date_mod` timestamp NULL DEFAULT NULL,
                  `date_creation` timestamp NULL DEFAULT NULL,
                  `wid` BIGINT DEFAULT NULL COMMENT 'Wunderlist id',
                  `type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Wundrlist type',
                  `revision` int(11) DEFAULT NULL COMMENT 'Wunderlist revision',
                  `user_id` int(11) DEFAULT NULL COMMENT 'Wunderlist user_id',
                  PRIMARY KEY (`id`),
                  UNIQUE KEY (`users_id`),
                  UNIQUE KEY (`wid`),
                  KEY `date_mod` (`date_mod`),
                  KEY `date_creation` (`date_creation`),
                  KEY `type` (`type`),
                  KEY `revision` (`revision`),
                  KEY `user_id` (`user_id`)
                ) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci";
      $DB->queryOrDie($query);
   }

   if (!TableExists($t_users)) {
      $query = "CREATE TABLE `$t_users` (
                  `id` int(11) NOT NULL AUTO_INCREMENT,
                  `users_id` int(11) NOT NULL DEFAULT '0',
                  `date_mod` timestamp NULL DEFAULT NULL,
                  `date_creation` timestamp NULL DEFAULT NULL COMMENT 'Wunderlist created_at',
                  `wid` BIGINT DEFAULT NULL COMMENT 'Wunderlist id',
                  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Wunderlist name',
                  `comment` text COLLATE utf8_unicode_ci,
                  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Wunderlist email',
                  `revision` int(11) DEFAULT NULL COMMENT 'Wunderlist revision',
                  PRIMARY KEY (`id`),
                  UNIQUE KEY (`users_id`),
                  UNIQUE KEY (`wid`),
                  KEY `date_mod` (`date_mod`),
                  KEY `date_creation` (`date_creation`),
                  KEY `name` (`name`),
                  KEY `email` (`email`),
                  KEY `revision` (`revision`)
                ) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci";
      $DB->queryOrDie($query);
   }

   if (!TableExists($t_lists)) {
      $query = "CREATE TABLE `$t_lists` (
                  `id` int(11) NOT NULL AUTO_INCREMENT,
                  `users_id` int(11) NOT NULL DEFAULT '0',
                  `date_mod` timestamp NULL DEFAULT NULL,
                  `date_creation` timestamp NULL DEFAULT NULL COMMENT 'Wunderlist field created_at',
                  `wid` BIGINT DEFAULT NULL COMMENT 'Wunderlist field id',
                  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Wunderlist field title',
                  `comment` text COLLATE utf8_unicode_ci,
                  `list_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Wunderlist field list_type',
                  `type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Wunderlist field type',
                  `revision` int(11) DEFAULT NULL COMMENT 'Wunderlist field revision',
                  PRIMARY KEY (`id`),
                  UNIQUE KEY `users_list_id` (`users_id`, `wid`),
                  KEY `users_id` (`users_id`),
                  KEY `date_mod` (`date_mod`),
                  KEY `date_creation` (`date_creation`),
                  KEY `wid` (`wid`),
                  KEY `name` (`name`),
                  KEY `list_type` (`list_type`),
                  KEY `type` (`type`),
                  KEY `revision` (`revision`)
                ) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci";
      $DB->queryOrDie($query);
   }

   if (!TableExists($t_tasks)) {
      $query = "CREATE TABLE `$t_tasks` (
                  `id` int(11) NOT NULL AUTO_INCREMENT,
                  `itemtype` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
                  `items_id` int(11) NOT NULL DEFAULT '0',
                  `users_id` int(11) NOT NULL DEFAULT '0',
                  `date_mod` timestamp NULL DEFAULT NULL,
                  `date_creation` timestamp NULL DEFAULT NULL COMMENT 'Wunderlist field created_at',
                  `wid` BIGINT DEFAULT NULL COMMENT 'Wunderlist field id',
                  `assignee_id` int(11) DEFAULT NULL COMMENT 'Wunderlsit field assignee_id',
                  `created_by_id` int(11) DEFAULT NULL COMMENT 'Wumderlist field created_by_id',
                  `due_date` date DEFAULT NULL COMMENT 'Wunderlist field due_data',
                  `list_id` int(11) DEFAULT NULL COMMENT 'Wunderlist field list_id',
                  `revision` int(11) DEFAULT NULL COMMENT 'Wunderlist field revision',
                  `starred` bool DEFAULT NULL COMMENT 'Wunderlist field starred',
                  `completed` bool DEFAULT NULL COMMENT 'Wunderlist field completed',
                  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Wunderlist field name',
                  `comment` text COLLATE utf8_unicode_ci,
                  PRIMARY KEY (`id`),
                  UNIQUE KEY `users_id_wid` (`users_id`, `wid`),
                  UNIQUE KEY `itemtype_items_id` (`itemtype`, `items_id`),
                  KEY `itemtype` (`itemtype`),
                  KEY `items_id` (`items_id`),
                  KEY `users_id` (`users_id`),
                  KEY `wid` (`wid`),
                  KEY `date_mod` (`date_mod`),
                  KEY `date_creation` (`date_creation`),
                  KEY `assignee_id` (`assignee_id`),
                  KEY `created_by_id` (`created_by_id`),
                  KEY `due_date` (`due_date`),
                  KEY `list_id` (`list_id`),
                  KEY `revision` (`revision`),
                  KEY `starred` (`starred`),
                  KEY `completed` (`completed`),
                  KEY `name` (`name`)
                ) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci";
      $DB->queryOrDie($query);
   }

   if (!TableExists($t_settings)) {
      $query = "CREATE TABLE `$t_settings` (
                  `id` int(11) NOT NULL AUTO_INCREMENT,
                  `users_id` int(11) NOT NULL DEFAULT '0',
                  `date_mod` timestamp NULL DEFAULT NULL,
                  `date_creation` timestamp NULL DEFAULT NULL,
                  `tickettasks_list` int(11) DEFAULT NULL,
                  `projecttasks_list` int(11) DEFAULT NULL,
                  `problemtasks_list` int(11) DEFAULT NULL,
                  `changetasks_list` int(11) DEFAULT NULL,
                  `reminders_list` int(11) DEFAULT NULL,
                  PRIMARY KEY (`id`),
                  UNIQUE KEY (`users_id`),
                  KEY `date_mod` (`date_mod`),
                  KEY `date_creation` (`date_creation`),
                  KEY `tickettasks_list` (`tickettasks_list`),
                  KEY `projecttasks_list` (`projecttasks_list`),
                  KEY `problemtasks_list` (`problemtasks_list`),
                  KEY `changetasks_list` (`changetasks_list`),
                  KEY `reminders_list` (`reminders_list`)
                ) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci";
      $DB->queryOrDie($query);
   }

   return true;
}

/**
 * Plugin uninstall process
 *
 * @return boolean
 */
function plugin_wunderlist_uninstall() {

   global $DB;

   $tables = [
      'glpi_plugin_wunderlist_auths',
      'glpi_plugin_wunderlist_roots',
      'glpi_plugin_wunderlist_users',
      'glpi_plugin_wunderlist_lists',
      'glpi_plugin_wunderlist_tasks',
      'glpi_plugin_wunderlist_settings',
   ];

   foreach ($tables as $table) {
      if (TableExists($table)) {
          $query = "DROP TABLE `$table`";
          $DB->queryOrDie($query);
      }
   }
   $cfg = new PluginWunderlistConfig();
   $cfg->delAll();

   return true;

}
