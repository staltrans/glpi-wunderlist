<?php

include __DIR__ . '/../../../inc/includes.php';

$list = new PluginWunderlistList();
$settings = new PluginWunderlistSettings();

$fields = [
   'tickettasks_list',
   'projecttasks_list',
   'problemtasks_list',
   'changetasks_list',
   'reminders_list',
];

$lists_ids = [];

foreach ($settings->find() as $item) {
   foreach ($fields as $field) {
      if (!empty($item[$field])) {
         $lists_ids[] = $item[$field];
      }
   }
}

$active_task = [];
$condition = ' `id` IN (' . join(',', $lists_ids) . ')';

foreach ($list->find($condition) as $item) {
   $list_revision = $item['revision'];
   unset($item['revision']);
   $list->update($item);
   if ($list_revision < $list->fields['revision']) {
      $task = new PluginWunderlistTask();
      $condition = "`list_id`='{$item['wid']}' AND `completed`=0";
      foreach ($task->find($condition) as $data) {
         unset($data['revision']);
         $task->update($data);
      }
   }
}
