<?php

/**
 -------------------------------------------------------------------------
 wunderlist plugin for GLPI
 Copyright (C) 2017 by the wunderlist Development Team.

 https://bitbucket.org/staltrans/wunderlist
 -------------------------------------------------------------------------

 LICENSE

 This file is part of wunderlist.

 wunderlist is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 wunderlist is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with wunderlist. If not, see <http://www.gnu.org/licenses/>.
 --------------------------------------------------------------------------
 */

include __DIR__ . '/../../../inc/includes.php';

Session::checkLoginUser();

$auth = new PluginWunderlistAuth();

if (!empty($_GET)) {
   $auth->setAccessToken($_GET);
}

if (!empty($_POST)) {
   $auth->save($_POST);
}

if ($auth::canView()) {
   Html::header($auth->getMenuName(), $_SERVER['PHP_SELF'], 'plugins', 'pluginwunderlistauth');
   $auth->display();
   Html::footer();
} else {
   Html::displayRightError();
}
