<?php

/**
 -------------------------------------------------------------------------
 wunderlist plugin for GLPI
 Copyright (C) 2017 by the wunderlist Development Team.

 https://bitbucket.org/staltrans/wunderlist
 -------------------------------------------------------------------------

 LICENSE

 This file is part of wunderlist.

 wunderlist is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 wunderlist is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with wunderlist. If not, see <http://www.gnu.org/licenses/>.
 --------------------------------------------------------------------------
 */

class PluginWunderlistList extends PluginWunderlistCommonDropdown {

   static $rightname = 'plugin_wunderlist_list';

   protected $field_map = [
      // wunderlist   // glpi database
      'id'         => 'wid',
      'created_at' => 'date_creation',
      'title'      => 'name',
      'list_type'  => 'list_type',
      'type'       => 'type',
      'revision'   => 'revision',
   ];

   static function getTypeName($nb = 0) {
      return __('Список Wunderlist', 'wunderlist');
   }

   function getWunderlistList($list_id = null) {
      $api = new \Wunderlist\Api($this->client_id, $this->access_token);
      $resp = $api->getList($list_id);
      if ($resp->code == '200') {
         $result = [];
         if (isset($list_id)) {
            foreach($resp->body as $field => $val) {
               if (isset($this->field_map[$field])) {
                  $result[$this->field_map[$field]] = $val;
               }
            }
         } else {
            foreach($resp->body as $key => $item) {
               foreach($item as $field => $val) {
                  if (isset($this->field_map[$field])) {
                     $result[$item->id][$this->field_map[$field]] = $val;
                  }
               }
            }
         }
         return $result;
      }
      return false;
   }

   function newWunderlistList($title) {
      $api = new \Wunderlist\Api($this->client_id, $this->access_token);
      $resp = $api->newList($title);
      if ($resp->code == '201') {
         $result = [];
         foreach($resp->body as $key => $val) {
            $result[$this->field_map[$key]] = $val;
         }
         return $result;
      }
      return false;
   }

   function updWunderlistList($list_id, $revision, $title = null) {
      $api = new \Wunderlist\Api($this->client_id, $this->access_token);
      $resp = $api->updList($list_id, $revision, $title);
      if ($resp->code == '200') {
         $result = [];
         foreach($resp->body as $key => $val) {
            $result[$this->field_map[$key]] = $val;
         }
         return $result;
      }
      return false;
   }

   function delWunderlistList($list_id, $revision) {
      $api = new \Wunderlist\Api($this->client_id, $this->access_token);
      $resp = $api->delList($list_id, $revision);
      if ($resp->code == '204' || $resp->code == '404') {
         return true;
      }
      return false;
   }

   function prepareInputForAdd($input) {
      if (empty($input[$this->field_map['title']])) {
         return false;
      }
      if (empty($input['users_id'])) {
         $input['users_id'] = Session::getLoginUserID();
      }
      if(empty($input['users_id'])) {
         return false;
      }
      if($this->isEmptyWunderlistFields($input)) {
         $oauth = new PluginWunderlistAuth();
         $data = $oauth->getFromDBByUserID($input['users_id']);
         if ($data) {
            $this->setAccessToken($oauth->fields['access_token']);
            $resp = $this->newWunderlistList($input[$this->field_map['title']]);
            if ($resp) {
               $input = array_merge($input, $resp);
            } else {
               return false;
            }
         } else {
            return false;
         }
      }
      return $input;
   }

   function prepareInputForUpdate($input) {
      if (empty($input['users_id'])) {
         $input['users_id'] = Session::getLoginUserID();
      }
      if(empty($input['users_id'])) {
         return false;
      }
      if($this->isEmptyWunderlistFields($input)) {
         $oauth = new PluginWunderlistAuth();
         $data = $oauth->getFromDBByUserID($input['users_id']);
         if ($data) {
            $this->setAccessToken($oauth->fields['access_token']);
            if (empty($input[$this->field_map['revision']])) {
               $resp = $this->getWunderlistList($input[$this->field_map['id']]);
               if ($resp) {
                  $input[$this->field_map['revision']] = $resp[$this->field_map['revision']];
               } else {
                  return false;
               }
            }
            $resp = $this->updWunderlistList($input[$this->field_map['id']], $input[$this->field_map['revision']], $input[$this->field_map['title']]);
            if ($resp) {
               $input = array_merge($input, $resp);
            } else {
               return false;
            }
         } else {
            return false;
         }
      }
      return $input;
   }

   function pre_deleteItem() {
      $oauth = new PluginWunderlistAuth();
      $data = $oauth->getFromDBByUserID($this->fields['users_id']);
      if ($data) {
         $this->setAccessToken($oauth->fields['access_token']);
         return $this->delWunderlistList($this->fields[$this->field_map['id']], $this->fields[$this->field_map['revision']]);
      } else {
         return false;
      }
   }

   function syncWunderlistLists($users_id = null) {
      global $DB;
      if (!isset($users_id)) {
         $users_id = Session::getLoginUserID();
      }
      $oauth = new PluginWunderlistAuth();
      $data = $oauth->getFromDBByUserID($users_id);
      if ($data) {
         $this->setAccessToken($oauth->fields['access_token']);
         $resp = $this->getWunderlistList();
         if ($resp) {
            $lists_ids = join(',', array_keys($resp));
            $condition = "`wid` IN ($lists_ids) AND `users_id`='$users_id'";
            $result = $this->find($condition);
            if (!empty($result)) {
               foreach($result as $val) {
                  $id = $this->field_map['id'];
                  $rev = $this->field_map['revision'];
                  if ($val[$rev] < $resp[$val[$id]]['revision']) {
                     $input = array_merge($val, $resp[$val[$id]]);
                     $this->update($input);
                  }
                  unset($resp[$val[$id]]);
               }
            }
            if (!empty($resp)) {
               foreach($resp as $val) {
                  $val['users_id'] = $users_id;
                  $this->add($val);
               }
            }
            $query = "DELETE FROM `{$this->getTable()}` WHERE `wid` NOT IN ($lists_ids) AND `users_id`='$users_id'";
            $DB->query($query);
         }
      }
   }

   function getSearchOptionsNew() {

      $tab = [];

      $tab[] = [
          'id'   => 'common',
          'name' => __('Списки Wunderlist', 'wunderlist')
      ];

      $i = 1;

      $tab[] = [
         'id'            => $i++,
         'table'         => $this->getTable(),
         'field'         => $this->field_map['title'],
         'name'          => __('Название', 'wunderlist'),
         'datatype'      => 'itemlink',
         'massiveaction' => false
      ];

      $tab[] = [
         'id'            => $i++,
         'table'         => $this->getTable(),
         'field'         => $this->field_map['list_type'],
         'name'          => __('Тип', 'wunderlist'),
         'datatype'      => 'string',
         'massiveaction' => false
      ];

      $tab[] = [
         'id'            => $i++,
         'table'         => $this->getTable(),
         'field'         => $this->field_map['revision'],
         'name'          => __('Ревизия', 'wunderlist'),
         'datatype'      => 'string',
         'massiveaction' => false
      ];

      $tab[] = [
         'id'            => $i++,
         'table'         => $this->getTable(),
         'field'         => $this->field_map['created_at'],
         'name'          => __('Дата создания', 'wunderlist'),
         'datatype'      => 'string',
         'massiveaction' => false
      ];

      $tab[] = [
         'id'            => $i++,
         'table'         => $this->getTable(),
         'field'         => $this->field_map['id'],
         'name'          => __('Идентификатор', 'wunderlist'),
         'datatype'      => 'itemlink',
         'massiveaction' => false
      ];

      $tab[] = [
         'id'            => $i++,
         'table'         => $this->getTable(),
         'field'         => $this->field_map['type'],
         'name'          => __('Тип объекта', 'wunderlist'),
         'datatype'      => 'string',
         'massiveaction' => false
      ];

      $tab[] = [
         'id'            => $i++,
         'table'         => $this->getTable(),
         'field'         => 'users_id',
         'name'          => __('User'),
         'datatype'      => 'user',
         'massiveaction' => false
      ];

      return $tab;
   }

}
