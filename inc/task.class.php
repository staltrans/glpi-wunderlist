<?php

/**
 -------------------------------------------------------------------------
 wunderlist plugin for GLPI
 Copyright (C) 2017 by the wunderlist Development Team.

 https://bitbucket.org/staltrans/wunderlist
 -------------------------------------------------------------------------

 LICENSE

 This file is part of wunderlist.

 wunderlist is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 wunderlist is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with wunderlist. If not, see <http://www.gnu.org/licenses/>.
 --------------------------------------------------------------------------
 */

class PluginWunderlistTask extends PluginWunderlistCommonDBTM {

   static $rightname = 'plugin_wunderlist_task';

   protected $field_map = [
      'id' => 'wid',
      'assignee_id' => 'assignee_id',
      'created_at' => 'date_creation',
      'created_by_id' => 'created_by_id',
      'due_date' => 'due_date',
      'list_id' => 'list_id',
      'revision' => 'revision',
      'starred' => 'starred',
      'title' => 'name',
      'completed' => 'completed',
   ];

   private function periods() {
      return [
         'day' => __('Day'),
         'week' => __('Week'),
         'month' => __('Month'),
         'year' => __('Year')
      ];
   }

   static function getTypeName($nb = 0) {
      return __('Задачи Wunderlist', 'wunderlist');
   }

   function getWunderlistTasks($list_id, $completed = null) {
      $api = new \Wunderlist\Api($this->client_id, $this->access_token);
      $resp = $api->getTasks($list_id, $completed);
      if ($resp->code == '200') {
         $result = [];
         foreach($resp->body as $key => $item) {
            foreach($item as $field => $val) {
               if (isset($this->field_map[$field])) {
                  $result[$item->id][$this->field_map[$field]] = $val;
               }
            }
         }
         return $result;
      }
      return false;
   }

   function getWunderlistTask($task_id) {
      $api = new \Wunderlist\Api($this->client_id, $this->access_token);
      $resp = $api->getTask($task_id);
      if ($resp->code == '200') {
         $result = [];
         foreach($resp->body as $key => $val) {
            if (isset($this->field_map[$key])) {
               $result[$this->field_map[$key]] = $val;
            }
         }
         return $result;
      }
      return false;
   }

   function newWunderlistTask($list_id, $title, $other_vars = []) {
      $api = new \Wunderlist\Api($this->client_id, $this->access_token);
      $resp = $api->newTask($list_id, $title, $other_vars);
      if ($resp->code == '201') {
         $result = [];
         foreach($resp->body as $key => $val) {
            $result[$this->field_map[$key]] = $val;
         }
         return $result;
      }
      return false;
   }

   function updWunderlistTask($task_id, $revision, $other_vars = []) {
      $api = new \Wunderlist\Api($this->client_id, $this->access_token);
      $resp = $api->updTask($task_id, $revision, $other_vars);
      if ($resp->code == '200') {
         $result = [];
         foreach($resp->body as $key => $val) {
            $result[$this->field_map[$key]] = $val;
         }
         return $result;
      }
      return false;
   }

   function delWunderlistTask($task_id, $revision) {
      $api = new \Wunderlist\Api($this->client_id, $this->access_token);
      $resp = $api->delTask($task_id, $revision);
      if ($resp->code == '204' || $resp->code == '404') {
         return true;
      }
      return false;
   }

   function newWunderlistTaskComments($task_id, $text) {
      $api = new \Wunderlist\Api($this->client_id, $this->access_token);
      $resp = $api->newTaskComments($task_id, $text);
      if ($resp->code == '201') {
         return true;
      }
      return false;
   }

   function getFromDBByItemsID($itemtype, $items_id) {
      $q = "WHERE `itemtype`='$itemtype' AND `items_id`='$items_id'";
      return $this->getFromDBByQuery($q);
   }

   function prepareInputForAdd($input) {
      if (empty($input[$this->field_map['title']])) {
         return false;
      }
      if (empty($input[$this->field_map['list_id']])) {
         return false;
      }
      if (empty($input['users_id'])) {
         $input['users_id'] = Session::getLoginUserID();
      }
      if(empty($input['users_id'])) {
         return false;
      }
      $oauth = new PluginWunderlistAuth();
      $data = $oauth->getFromDBByUserID($input['users_id']);
      if ($data) {
         $this->setAccessToken($oauth->fields['access_token']);
         $opt = [];
         $key = $this->field_map['completed'];
         if (!empty($input[$key])) {
            $opt['completed'] = $input[$key];
         }
         $key = $this->field_map['recurrence_type'];
         if (!empty($input[$key]) && in_array($input[$key], array_keys($this->periods()))) {
            $opt['recurrence_type'] = $input[$key];
         }
         $key = $this->field_map['recurrence_count'];
         if (empty($input[$key])) {
            if (isset($opt['recurrence_type'])) {
               $opt['recurrence_count'] = 1;
            }
         } else {
            if ($input[$key] >= 1) {
               $opt['recurrence_count'] = $input[$key];
            } else {
               $opt['recurrence_count'] = 1;
            }
         }
         $key = $this->field_map['due_date'];
         if (!empty($input[$key]) && strtotime($input[$key])) {
            $opt['due_date'] = $input[$key];
         }
         $key = $this->field_map['starred'];
         if (!empty($input[$key]) && is_bool($input[$key])) {
            $opt['starred'] = $input[$key];
         }
         $key = $this->field_map['assignee_id'];
         if (!empty($input[$key])) {
            $opt['assignee_id'] = intval($input[$key]);
         }
         $resp = $this->newWunderlistTask($input[$this->field_map['list_id']], $input[$this->field_map['title']], $opt);
         if ($resp) {
            $input = array_merge($input, $resp);
            if(!empty($input['comment'])) {
               $this->newWunderlistTaskComments($input[$this->field_map['id']], $input['comment']);
            }
         } else {
            return false;
         }
      } else {
         return false;
      }
      return $input;
   }

   function prepareInputForUpdate($input) {
      if (empty($input[$this->field_map['id']])) {
         return false;
      }
      if (empty($input['users_id'])) {
         $input['users_id'] = Session::getLoginUserID();
      }
      if(empty($input['users_id'])) {
         return false;
      }
      $oauth = new PluginWunderlistAuth();
      $data = $oauth->getFromDBByUserID($input['users_id']);
      if ($data) {
         $this->setAccessToken($oauth->fields['access_token']);
         $opt = [];
         $key = $this->field_map['title'];
         if (!empty($input[$key])) {
            $opt['title'] = $input[$key];
         }
         $key = $this->field_map['list_id'];
         if (!empty($input[$key])) {
            $opt['list_id'] = $input[$key];
         }
         $key = $this->field_map['completed'];
         if (!empty($input[$key])) {
            $opt['completed'] = $input[$key];
         }
         $key = $this->field_map['recurrence_type'];
         if (!empty($input[$key]) && in_array($input[$key], array_keys($this->periods()))) {
            $opt['recurrence_type'] = $input[$key];
         }
         $key = $this->field_map['recurrence_count'];
         if (empty($input[$key])) {
            if (isset($opt['recurrence_type'])) {
               $opt['recurrence_count'] = 1;
            }
         } else {
            if ($input[$key] >= 1) {
               $opt['recurrence_count'] = $input[$key];
            } else {
               $opt['recurrence_count'] = 1;
            }
         }
         $key = $this->field_map['due_date'];
         if (!empty($input[$key]) && strtotime($input[$key])) {
            $opt['due_date'] = $input[$key];
         }
         $key = $this->field_map['starred'];
         if (!empty($input[$key]) && is_bool($input[$key])) {
            $opt['starred'] = $input[$key];
         }
         if (empty($input[$this->field_map['revision']])) {
            $resp = $this->getWunderlistTask($input[$this->field_map['id']]);
            if ($resp) {
               $input[$this->field_map['revision']] = $resp[$this->field_map['revision']];
            } else {
               return false;
            }
         }
         $resp = $this->updWunderlistTask($input[$this->field_map['id']], $input[$this->field_map['revision']], $opt);
         if ($resp) {
            $input = array_merge($input, $resp);
         } else {
            return false;
         }
      } else {
         return false;
      }
      return $input;
   }

   function pre_deleteItem() {
      $oauth = new PluginWunderlistAuth();
      $data = $oauth->getFromDBByUserID($this->fields['users_id']);
      if ($data) {
         $this->setAccessToken($oauth->fields['access_token']);
         $resp = $this->getWunderlistTask($this->fields[$this->field_map['id']]);
         if ($resp) {
            $this->fields[$this->field_map['revision']] = $resp[$this->field_map['revision']];
         } else {
            return false;
         }
         return $this->delWunderlistTask($this->fields[$this->field_map['id']], $this->fields[$this->field_map['revision']]);
      } else {
         return false;
      }
   }

   function post_updateItem($history = 1) {
      if (isset($this->oldvalues['completed']) && $this->fields['completed']) {
         if (in_array($this->fields['itemtype'], ['TicketTask', 'ProblemTask', 'ChangeTask', 'Reminder'])) {
            $item = new $this->fields['itemtype'];
            $item->getFromDB($this->fields['items_id']);
            $item->fields['state'] = Planning::DONE;
            $item->update($item->fields);
         }
      }
   }

}
