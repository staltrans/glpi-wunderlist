<?php

/**
 -------------------------------------------------------------------------
 wunderlist plugin for GLPI
 Copyright (C) 2017 by the wunderlist Development Team.

 https://bitbucket.org/staltrans/wunderlist
 -------------------------------------------------------------------------

 LICENSE

 This file is part of wunderlist.

 wunderlist is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 wunderlist is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with wunderlist. If not, see <http://www.gnu.org/licenses/>.
 --------------------------------------------------------------------------
 */

class PluginWunderlistAuth extends CommonDBTM {

   public $dohistory         = true;
   public $history_blacklist = ['date_mod'];

   static $rightname = 'plugin_wunderlist_auth';

   static function getTypeName($nb = 0) {
      return __('Wunderlist OAuth', 'wunderlist');
   }

   static function getAdditionalMenuLinks() {
      return ['config' => PluginWunderlistConfig::getFormURL()];
   }

   function prepareInputForAdd($input) {
      if (empty($input['state'])) {
         $input['state'] = $this->genString();
      }
      return $input;
   }

   function getSearchOptionsNew() {

      $tab = [];

      $tab[] = [
          'id'   => 'common',
          'name' => __('Токен Wunderlist OAuth', 'wunderlist'),
      ];

      $i = 1;

      $tab[] = [
         'id'            => $i++,
         'table'         => $this->getTable(),
         'field'         => 'users_id',
         'name'          => __('User'),
         'massiveaction' => false,
      ];

      $tab[] = [
         'id'            => $i++,
         'table'         => $this->getTable(),
         'field'         => 'id',
         'name'          => __('ID'),
         'datatype'      => 'itemlink',
         'massiveaction' => false,
      ];

      $tab[] = [
         'id'            => $i++,
         'table'         => $this->getTable(),
         'field'         => 'state',
         'name'          => __('State'),
         'massiveaction' => false,
      ];

      $tab[] = [
         'id' => $i++,
         'table' => $this->getTable(),
         'field' => 'code',
         'name' => __('Code'),
         'massiveaction' => false,
      ];

      $tab[] = [
         'id' => $i++,
         'table' => $this->getTable(),
         'field' => 'access_token',
         'name' => __('Access token', 'wunderlist'),
         'massiveaction' => false,
      ];

      return $tab;
   }

   static function getSpecificValueToDisplay($field, $values, array $options = []) {

      if (!is_array($values)) {
         $values = [$field => $values];
      }
      switch ($field) {
         case 'users_id':
            $user = new User();
            $user->getFromDB($values[$field]);
            return $user->getLink();
      }
      return parent::getSpecificValueToDisplay($field, $values, $options);
   }

   function getFromDBByUserID($users_id) {
      $q = "WHERE `users_id` = '" . Toolbox::cleanInteger($users_id) . "'";
      return $this->getFromDBByQuery($q);
   }

   function getFromDBByUserState($users_id, $state) {
      global $DB;
      $q = "WHERE `users_id` = '" . Toolbox::cleanInteger($users_id) . "' AND `state` = '" . $DB->escape($state) . "'";
      return $this->getFromDBByQuery($q);
   }

   function defineTabs($options = []) {

      $ong = [];
      $this->addDefaultFormTab($ong);
      $this->addStandardTab('Log', $ong, $options);
      return $ong;

   }

   function showForm($ID, $options = []) {

      $this->initForm($ID, $options);
      $this->showFormHeader($options);
      echo '<tr><td>' . __('User') . '</td>';
      echo '<td>';
      $opt = [
         'value' => $this->fields['users_id'],
         'display' => false,
         'right' => 'all',
         'width' => '50%',
         'specific_tags' => ['readonly' => 'readonly'],
      ];
      echo User::dropdown($opt);
      echo '</td></tr>';
      echo '<tr><td>' . __('State') . '</td>';
      echo '<td>';
      echo Html::autocompletionTextField($this, 'state', ['option' => 'readonly="readonly" size="32"']);
      echo '</td></tr>';
      echo '<tr><td>' . __('Code') . '</td>';
      echo '<td>';
      echo Html::autocompletionTextField($this, 'code', ['option' => 'readonly="readonly" size="32"']);
      echo '</td></tr>';
      echo '<tr><td>' . __('Access Token') . '</td>';
      echo '<td>';
      echo Html::autocompletionTextField($this, 'access_token', ['option' => 'readonly="readonly" size="64"']);
      echo '</td></tr>';
      $this->showFormButtons($options);

      return true;
   }

  function save($post = []) {
      if (isset($post['add'])) {
         //$this->check(-1, CREATE, $post);
         //$this->add($post);
      } elseif (isset($post['purge'])) {
         $this->check($post['id'], PURGE);
         $this->delete($post, 1);
         $this->redirectToList();
      } elseif (isset($post['update'])) {
         //$this->check($post['id'], UPDATE);
         //$this->update($post);
      }

   }

   function getTabNameForItem(CommonGLPI $item, $withtemplate = 0) {
      if ($item->getType() == 'Preference') {
         return __('Wunderlist', 'wunderlist');
      }
      return;
   }

   static function displayTabContentForItem(CommonGLPI $item, $tabnum = 1, $withtemplate = 0) {

      if ($item->getType() == 'Preference') {
         $me = new self();
         $me->showOAuthForm();
      }

      return true;

   }

   function showOAuthForm() {
      global $CFG_GLPI;
      $input = ['users_id' => Session::getLoginUserID()];
      if ($input['users_id']) {
         $data = $this->getFromDBByUserID($input['users_id']);
         if($data) {
            $wuser = new PluginWunderlistUser();
            if ($wuser->getFromDBbyUserID($input['users_id'])) {
               echo '<div class="center">';
               echo '<table class="tab_cadre_fixe"><tbody>';
               echo '<tr><th colspan="2">' . __('Wunderlist account', 'wunderlist') . '</th></tr>';
               echo '<tr><td rowspan="4" width="75px">' . $wuser->getAvatar() . '</td><td><strong>' . $wuser->fields['name'] . '</strong></td></tr>';
               echo '<tr><td>' . $wuser->fields['email'] . '</td></tr>';
               echo '<tr><td>' . sprintf(__('<a href="%s">My Wunderlist</a>', 'wunderlist'), 'https://www.wunderlist.com/webapp') . '</td></tr>';
               echo '<tr><td>' . sprintf(__('<a href="%s">Wunserlist account prefences</a>', 'wunderlist'), 'https://www.wunderlist.com/webapp/#/preferences/account') . '</td></tr>';
               echo '</tbody></table>';

               $settings = new PluginWunderlistSettings($input['users_id']);
               $settings->showFormSettings();

            } else {
               $wuser->add($input);
                Html::redirect($CFG_GLPI['root_doc'] . '/front/preference.php?forcetab=' . $this->getType() . '$1');
            }
         } else {
            $this->add($input);
         }
         if (empty($this->fields['access_token'])) {
            $cfg = new PluginWunderlistConfig();
            $client_id = $cfg->get('client_id');
            if (!empty($client_id)) {
               $oauth = new \Wunderlist\Auth($client_id);
               $callback = $CFG_GLPI['url_base'] . self::getFormURL();
               echo '<div class="center"><a class="vsubmit" href="' . $oauth->reuqestAccessUrl($callback, $this->fields['state']) . '">' . __('Wunderlist OAuth', 'wunderlist') . '</a></div>';
            } else {
              echo '<div class="center">' . sprintf(__('Plese <a href="%s">configure wunderlist module</a>'), PluginWunderlistConfig::getFormURL()) . '</div>';
            }
         }
      }
   }

   function setAccessToken($get) {
      global $CFG_GLPI;
      if (!empty($get['state']) && !empty($get['code'])) {
         $users_id = Session::getLoginUserID();
         if ($users_id) {
            $data = $this->getFromDBByUserState($users_id, $get['state']);
            if ($data) {
               $cfg = new PluginWunderlistConfig();
               $client_id = $cfg->get('client_id');
               $client_secret = $cfg->get('client_secret');
               if (!empty($client_id) && !empty($client_secret)) {
                  $oauth = new \Wunderlist\Auth($client_id);
                  $input = $this->fields;
                  $input['code'] = $get['code'];
                  $input['access_token'] = $oauth->getAccessToken($client_secret, $get['code']);
                  $this->update($input);
               }
            } else {
               Html::displayRightError();
            }
         }
         Html::redirect($CFG_GLPI['root_doc'] . '/front/preference.php?forcetab=' . $this->getType() . '$1');
      }
   }

   function genString($length = 32) {
      return Toolbox::getRandomString($length);
   }

}
