<?php

/**
 -------------------------------------------------------------------------
 wunderlist plugin for GLPI
 Copyright (C) 2017 by the wunderlist Development Team.

 https://bitbucket.org/staltrans/wunderlist
 -------------------------------------------------------------------------

 LICENSE

 This file is part of wunderlist.

 wunderlist is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 wunderlist is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with wunderlist. If not, see <http://www.gnu.org/licenses/>.
 --------------------------------------------------------------------------
 */

class PluginWunderlistCommonDBTM extends CommonDBTM {

   public $dohistory         = false;
   public $history_blacklist = ['date_mod'];

   protected $client_id;
   protected $access_token;

   protected $field_map = [];

   function __construct($access_token = null) {
      parent::__construct();
      $cfg = new PluginWunderlistConfig();
      $this->client_id = $cfg->get('client_id');
      $this->access_token = $access_token;
   }

   function setAccessToken($access_token) {
      $this->access_token = $access_token;
   }

   function getAccessToken() {
      return $this->access_token;
   }

   function isEmptyWunderlistFields($fields = []) {
      if (empty($this->field_map)) {
         return true;
      }
      foreach ($this->field_map as $key => $val) {
         if (empty($fields[$val])) {
            return true;
         }
      }
      return false;
   }

}
