<?php

/**
 -------------------------------------------------------------------------
 wunderlist plugin for GLPI
 Copyright (C) 2017 by the wunderlist Development Team.

 https://bitbucket.org/staltrans/wunderlist
 -------------------------------------------------------------------------

 LICENSE

 This file is part of wunderlist.

 wunderlist is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 wunderlist is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with wunderlist. If not, see <http://www.gnu.org/licenses/>.
 --------------------------------------------------------------------------
 */

class PluginWunderlistRoot extends PluginWunderlistCommonDBTM {

   static $rightname = 'plugin_wunderlist_root';

   protected $field_map = [
      // wunderlist // glpi database
      'id'       => 'wid',
      'type'     => 'type',
      'revision' => 'revision',
      'user_id'  => 'users_id',
   ];

   static function getTypeName($nb = 0) {
      return __('Wunderlist Root', 'wunderlist');
   }

   function getWunderlistRoot() {
      $api = new \Wunderlist\Api($this->client_id, $this->access_token);
      $resp = $api->getRoot();
      if ($resp->code == '200') {
         $result = [];
         foreach($resp->body as $key => $val) {
            if (isset($this->field_map[$key])) {
               $result[$this->field_map[$key]] = $val;
            }
         }
         return $result;
      }
      return false;
   }

   function prepareInputForAdd($input) {
      if (empty($input['users_id'])) {
         $input['users_id'] = Session::getLoginUserID();
      }
      if (empty($input['users_id'])) {
         return false;
      }
      if ($this->isEmptyWunderlistFields($input)) {
         $oauth = new PluginWunderlistAuth();
         $data = $oauth->getFromDBByUserID($input['users_id']);
         if ($data) {
            $this->setAccessToken($oauth->fields['access_token']);
            $resp = $this->getWunderlistRoot();
            if ($resp) {
               $input = array_merge($input, $resp);
            } else {
               return false;
            }
         } else {
            return false;
         }
      }
      return $input;
   }

   function prepareInputForUpdate($input) {
      return $this->prepareInputForAdd($input);
   }

}
