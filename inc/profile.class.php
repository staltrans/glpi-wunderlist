<?php

/**
 -------------------------------------------------------------------------
 wunderlist plugin for GLPI
 Copyright (C) 2017 by the wunderlist Development Team.

 https://bitbucket.org/staltrans/wunderlist
 -------------------------------------------------------------------------

 LICENSE

 This file is part of wunderlist.

 wunderlist is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 wunderlist is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with wunderlist. If not, see <http://www.gnu.org/licenses/>.
 --------------------------------------------------------------------------
 */

class PluginWunderlistProfile extends Profile {

   function getTabNameForItem(CommonGLPI $item, $withtemplate = 0) {
      if ($item->getType() == 'Profile') {
         return __('Wunderlist', 'wunderlist');
      }
      return '';
   }

   function getConfigRights() {
      return [
          [
            'itemtype' => 'PluginWunderlistConfig',
            'label'    => __(sprintf('Настройки <a href="%s">приложения Wunderlist</a>', 'https://developer.wunderlist.com/apps'), 'wunderlist'),
            'field'    => PluginWunderlistConfig::$rightname,
          ],
          [
            'itemtype' => 'PluginWunderlistAuth',
            'label'    => __('Wunderlist OAuth', 'wunderlist'),
            'field'    => PluginWunderlistAuth::$rightname,
          ],
          [
            'itemtype' => 'PluginWunderlistList',
            'label'    => __('Списки Wunderlist', 'wunderlist'),
            'field'    => PluginWunderlistList::$rightname,
          ],
          [
            'itemtype' => 'PluginWunderlistSettings',
            'label'    => __('Настройки Wunderlist', 'wunderlist'),
            'field'    => PluginWunderlistSettings::$rightname,
          ],

      ];
   }

   static function displayTabContentForItem(CommonGLPI $item, $tabnum = 1, $withtemplate = 0) {

      if ($item->getType() == 'Profile') {
         $pid = $item->getID();
         $me = new self();
         $me->showForm($pid);
      }
      return true;
   }

   function showForm($profiles_id = 0, $openform = TRUE, $closeform = TRUE) {

      if (!self::canView()) {
         return false;
      }

      $profile = new Profile();
      $canedit = Session::haveRightsOr(self::$rightname, [CREATE, UPDATE, PURGE]);

      echo '<div class="spaced">';

      if ($canedit && $openform) {
         echo '<form method="post" action="' . $profile->getFormURL() . '">';
      }

      $profile->getFromDB($profiles_id);

      $matrix_options = [
         'title'         => __('Настройки плагина', 'wunderlist'),
         'canedit'       => $canedit,
         'default_class' => 'tab_bg_2'
      ];

      $profile->displayRightsChoiceMatrix($this->getConfigRights(), $matrix_options);

      if ($canedit && $closeform) {
         echo '<div class="center">';
         echo Html::hidden('id', ['value' => $profiles_id]);
         echo Html::submit(_sx('button', 'Save'), ['name' => 'update']);
         echo '</div>'; // .center
         Html::closeForm();
      }

      echo '</div>'; // .spaced
   }

}
