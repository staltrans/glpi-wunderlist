<?php

/**
 -------------------------------------------------------------------------
 wunderlist plugin for GLPI
 Copyright (C) 2017 by the wunderlist Development Team.

 https://bitbucket.org/staltrans/wunderlist
 -------------------------------------------------------------------------

 LICENSE

 This file is part of wunderlist.

 wunderlist is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 wunderlist is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with wunderlist. If not, see <http://www.gnu.org/licenses/>.
 --------------------------------------------------------------------------
 */

class PluginWunderlistHook {

   static function truncateText($text) {
      return Html::resume_name($text, 40);
   }

   static function prepareCommonITILTask(CommonITILTask $item) {
      global $CFG_GLPI;
      $settings = new PluginWunderlistSettings();
      $list = new PluginWunderlistList();
      $input = [
         'itemtype' => $item->getType(),
         'items_id' => $item->fields['id'],
         'comment'  => Html::cleanPostForTextArea(Html::clean($item->fields['content'])),
      ];

      // $input['completed']
      if (!empty($item->fields['state']) && $item->fields['state'] == Planning::DONE) {
         $input['completed'] = true;
      }
      // $input['users_id']
      if(!empty($item->fields['users_id'])) {
         $input['users_id'] = $item->fields['users_id'];
      }
      // $input['assignee_id']
      $wuser = new PluginWunderlistUser();
      if (!empty($item->fields['users_id_tech'])) {
         if ($wuser->getFromDBbyUserID($item->fields['users_id_tech'])) {
            $input['assignee_id'] = $wuser->fields['wid'];
         }
      } elseif(!empty($input['users_id'])) {
         if ($wuser->getFromDBbyUserID($input['users_id'])) {
            $input['assignee_id'] = $wuser->fields['wid'];
         }
      }
      if (!empty($item->fields['end'])) {
         $input['due_date'] = $item->fields['end'];
      }
      $settings->getFromDBByUsersID($input['users_id']);
      // $input['name'] && $input['list_id']
      switch($input['itemtype']) {
         case 'TicketTask':
            $ticket = new Ticket();
            $ticket->getFromDB($item->fields['tickets_id']);
            $input['comment'] .= "\n" . $CFG_GLPI['url_base'] . $ticket->getLinkURL();
            $input['name'] = sprintf(__('Заявка №%s. %s', 'wunderlist'), $item->fields['tickets_id'], self::truncateText($input['comment']));
            $list->getFromDB($settings->fields['tickettasks_list']);
            $input['list_id'] = $list->fields['wid'];
            break;
         case 'ChangeTask':
            $change = new Change();
            $change->getFromDB($item->fields['changes_id']);
            $input['comment'] .= "\n" . $CFG_GLPI['url_base'] . $change->getLinkURL();
            $input['name'] = sprintf(__('Изменение №%s. %s', 'wunderlist'), $item->fields['changes_id'], self::truncateText($input['comment']));
            $list->getFromDB($settings->fields['changetasks_list']);
            $input['list_id'] = $list->fields['wid'];
            break;
         case 'ProblemTask':
            $problem = new Problem();
            $problem->getFromDB($item->fields['problems_id']);
            $input['comment'] .= "\n" . $CFG_GLPI['url_base'] . $problem->getLinkURL();
            $input['name'] = sprintf(__('Проблема №%s. %s', 'wunderlist'), $item->fields['problems_id'], self::truncateText($input['comment']));
            $list->getFromDB($settings->fields['problemtasks_list']);
            $input['list_id'] = $list->fields['wid'];
            break;
      }

      return $input;
   }

   static function prepareAddProjectTask(ProjectTask $item) {
      global $CFG_GLPI;
      $settings = new PluginWunderlistSettings();
      $list = new PluginWunderlistList();
      $input = [
         'itemtype' => $item->getType(),
         'items_id' => $item->fields['id']
      ];
      $input['comment'] = '';
      if (!empty($item->fields['content'])) {
         $input['comment'] .= Html::cleanPostForTextArea(Html::clean($item->fields['content']));
      } elseif(!empty($item->fields['comment'])) {
         $input['comment'] .= Html::cleanPostForTextArea(Html::clean($item->fields['comment']));
      }
      $input['comment'] .= "\n" . $CFG_GLPI['url_base'] . $item->getLinkURL();

      // $input['completed']
      // ???
      // $input['users_id']
      if(!empty($item->fields['users_id'])) {
         $wuser = new PluginWunderlistUser();
         $input['users_id'] = $item->fields['users_id'];
         if ($wuser->getFromDBbyUserID($input['users_id'])) {
            $input['assignee_id'] = $wuser->fields['wid'];
         }
      }
      if (!empty($item->fields['plan_end_date'])) {
         $input['due_date'] = $item->fields['plan_end_date'];
      } elseif (!empty($item->fields['real_end_date'])) {
         $input['due_date'] = $item->fields['real_end_date'];
      }
      $settings->getFromDBByUsersID($input['users_id']);
      // $input['name'] && $input['list_id']
      $input['name'] = $item->fields['name'];
      $list->getFromDB($settings->fields['projecttasks_list']);
      $input['list_id'] = $list->fields['wid'];
      return $input;
   }

   static function prepareAddReminder(Reminder $item) {
      global $CFG_GLPI;
      $settings = new PluginWunderlistSettings();
      $list = new PluginWunderlistList();
      $input = [
         'itemtype' => $item->getType(),
         'items_id' => $item->fields['id']
      ];
      $input['comment'] = '';
      if (!empty($item->fields['text'])) {
         $input['comment'] .= Html::cleanPostForTextArea(Html::clean($item->fields['text']));
      }
      $input['comment'] .= "\n" . $CFG_GLPI['url_base'] . $item->getLinkURL();

      // $input['completed']
      if (!empty($item->fields['state']) && $item->fields['state'] == Planning::DONE) {
         $input['completed'] = true;
      }
      // $input['users_id']
      if(!empty($item->fields['users_id'])) {
         $wuser = new PluginWunderlistUser();
         $input['users_id'] = $item->fields['users_id'];
         if ($wuser->getFromDBbyUserID($input['users_id'])) {
            $input['assignee_id'] = $wuser->fields['wid'];
         }
      }
      if (!empty($item->fields['end'])) {
         $input['due_date'] = $item->fields['end'];
      } elseif (!empty($item->fields['end_view_date'])) {
         $input['due_date'] = $item->fields['end_view_date'];
      }
      $settings->getFromDBByUsersID($input['users_id']);
      // $input['name'] && $input['list_id']
      $input['name'] = $item->fields['name'];
      $list->getFromDB($settings->fields['reminders_list']);
      $input['list_id'] = $list->fields['wid'];
      return $input;
   }

   static function itemAddCommonITILTask(CommonITILTask $item) {
      $task = new PluginWunderlistTask();
      $task->add(self::prepareCommonITILTask($item));
   }

   static function itemAddProjectTask(ProjectTask $item) {
      $task = new PluginWunderlistTask();
      $task->add(self::prepareAddProjectTask($item));
   }

   static function itemAddReminder(Reminder $item) {
      $task = new PluginWunderlistTask();
      $task->add(self::prepareAddReminder($item));
   }

   static function itemUpdateCommonITILTask(CommonITILTask $item) {
      $task = new PluginWunderlistTask();
      $task->getFromDBByItemsID($item->getType(), $item->fields['id']);
      $input = array_merge($task->fields, self::prepareCommonITILTask($item));
      unset($input['revision']);
      $task->update($input);
   }

   static function itemUpdateProjectTask(ProjectTask $item) {
      $task = new PluginWunderlistTask();
      $task->getFromDBByItemsID($item->getType(), $item->fields['id']);
      $input = array_merge($task->fields, self::prepareAddProjectTask($item));
      unset($input['revision']);
      $task->update($input);
   }

   static function itemUpdateReminder(Reminder $item) {
      $task = new PluginWunderlistTask();
      $task->getFromDBByItemsID($item->getType(), $item->fields['id']);
      $input = array_merge($task->fields, self::prepareAddReminder($item));
      unset($input['revision']);
      $task->update($input);
   }

   static function itemDelCommonITILTask(CommonITILTask $item) {
      $task = new PluginWunderlistTask();
      $task->getFromDBByItemsID($item->getType(), $item->fields['id']);
      $task->delete($task->fields);
   }

   static function itemDelProjectTask(ProjectTask $item) {
      $task = new PluginWunderlistTask();
      $task->getFromDBByItemsID($item->getType(), $item->fields['id']);
      $task->delete($task->fields);
   }

   static function itemDelReminder(Reminder $item) {
      $task = new PluginWunderlistTask();
      $task->getFromDBByItemsID($item->getType(), $item->fields['id']);
      $task->delete($task->fields);
   }

   static function test($item) {
      $dlog = fopen('/home/www/glpi/plugins/wunderlist/debug.log' , 'a');
      $a = "[" . date('Y/m/d H:i:s') . "] - - - - - - - - - - - - -\n";
      $a .= "Class: " . get_class($item) . "\n";
      $a .= "\$item =\n";
      $a .= print_r($item, true) . "\n";
      $a .= "\n";
      fwrite($dlog, $a);
      fclose($dlog);
   }

}
