<?php

/**
 -------------------------------------------------------------------------
 wunderlist plugin for GLPI
 Copyright (C) 2017 by the wunderlist Development Team.

 https://bitbucket.org/staltrans/wunderlist
 -------------------------------------------------------------------------

 LICENSE

 This file is part of wunderlist.

 wunderlist is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 wunderlist is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with wunderlist. If not, see <http://www.gnu.org/licenses/>.
 --------------------------------------------------------------------------
 */

class PluginWunderlistSettings extends CommonDBTM {

   public $history_blacklist = ['date_mod'];

   static $rightname = 'plugin_wunderlist_settings';

   static function getTypeName($nb = 0) {
      return __('Настройки Wunderlist', 'wunderlist');
   }

   function getFromDBByUsersID($users_id = null) {
      if (empty($users_id)) {
         $users_id = Session::getLoginUserID();
      }
      return $this->getFromDBByQuery("WHERE `users_id`='$users_id'");
   }

   function prepareInputForAdd($input) {
      if (!isset($input['users_id'])) {
         $input['users_id'] = Session::getLoginUserID();
      }
      return $input;
   }

   function prepareInputForUpdate($input) {
      $this->prepareInputForAdd($input);
      return $input;
   }

   function showFormSettings($users_id = null) {

      if (empty($users_id)) {
         $users_id = Session::getLoginUserID();
      }

      $opt = [
         'rand' => mt_rand(),
         'condition' => "`users_id`=$users_id",
      ];
      echo '<form name="form" method="post" action="' . $this->getFormURL() . '" enctype="multipart/form-data">';
      if ($this->getFromDBByUsersID($users_id)) {
         echo '<input type="hidden" name="id" value="' . $this->fields['id'] . '">';
      }

      echo '<div class="spaced" id="tabsbody">';
      echo '<table class="tab_cadre_fixe" id="mainformtable"></tbody>';

      $list = new PluginWunderlistList();

      echo '<tr><td>' . __('Список задач для заявок', 'wunderlist') . '</td><td>';
      $this->dropdownField($list, 'tickettasks_list', $opt);
      echo '</td></tr>';

      echo '<tr><td>' . __('Список задач для проектов', 'wunderlist') . '</td><td>';
      $this->dropdownField($list, 'projecttasks_list', $opt);
      echo '</td></tr>';

      echo '<tr><td>' . __('Список задач для проблем', 'wunderlist') . '</td><td>';
      $this->dropdownField($list, 'problemtasks_list', $opt);
      echo '</td></tr>';

      echo '<tr><td>' . __('Список задач для изменений', 'wunderlist') . '</td><td>';
      $this->dropdownField($list, 'changetasks_list', $opt);
      echo '</td></tr>';

      echo '<tr><td>' . __('Список задач для напоминаний', 'wunderlist') . '</td><td>';
      $this->dropdownField($list, 'reminders_list', $opt);
      echo '</td></tr>';

      echo '<tr><td colspan="2" class="center">';
      echo Html::submit(_x('button', 'Save'), ['name' => 'update_settings'] + $opt);
      echo '&nbsp;';
      echo Html::submit(_x('button', 'Получить списки', 'wunderlist'), ['name' => 'sync_list'] + $opt);
      echo '</td></tr>';

      echo '</tbody></table></div>';
      Html::closeForm();

   }

   private function dropdownField(CommonDBTM $item, $field_name, $opt = []) {
      $val = [];
      if (isset($this->fields[$field_name])) {
         $val = ['value' => $this->fields[$field_name]];
      }
      $item::dropdown(['name' => $field_name] + $val + $opt);
   }

   function saveSettings($post) {
      if (isset($post['update_settings'])) {
         if(isset($post['id'])) {
            $this->check($post['id'], UPDATE);
            $this->update($post);
         } else {
            $this->check(-1, CREATE, $post);
            $this->add($post);
         }
      } elseif (isset($post['sync_list'])) {
         $list = new PluginWunderlistList();
         $list->syncWunderlistLists();
      }
   }
}
