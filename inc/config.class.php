<?php

/**
 -------------------------------------------------------------------------
 wunderlist plugin for GLPI
 Copyright (C) 2017 by the wunderlist Development Team.

 https://bitbucket.org/staltrans/wunderlist
 -------------------------------------------------------------------------

 LICENSE

 This file is part of wunderlist.

 wunderlist is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 wunderlist is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with wunderlist. If not, see <http://www.gnu.org/licenses/>.
 --------------------------------------------------------------------------
 */

class PluginWunderlistConfig extends PluginConfigVariable {

   const VIEW_CONFIG      = 1024;
   const WRITE_CONFIG     = 2048;

   public $taborientation = 'vertical';
   static $rightname      = 'plugin_wunderlist_config';
   protected $displaylist = false;

   function __construct() {
      parent::__construct('wunderlist');
   }

   static function getTypeName($nb=0) {
      return __('Настройка Wubderlist', 'wunderlist');
   }

   static function canWrite() {
      return Session::haveRight(self::$rightname, self::WRITE_CONFIG);
   }

   static function canView() {
      return Session::haveRight(self::$rightname, self::VIEW_CONFIG) || self::canWrite();
   }

   static function canCreate() {
      return self::canView();
   }

   static function getMenuName() {
      return __('Wunderlist', 'wunderlist');
   }

   function getRights($interface = 'central') {
      return [
         self::VIEW_CONFIG   => __('View'),
         self::WRITE_CONFIG  => __('Edit'),
      ];
   }

   static function isNewID($ID) {
      return false;
   }

   static function getFromDB($ID) {
      return true;
   }

   function defineTabs($options = []) {

      $ong = [];
      $this->addStandardTab(__CLASS__, $ong, $options);

      return $ong;

   }

   function getTabNameForItem(CommonGLPI $item, $withtemplate = 0) {
      if ($item->getType() == __CLASS__) {
            $tabs[1] = __('Настройка приложения', 'wunderlist');
            return $tabs;
      }
      return '';
   }

   static function displayTabContentForItem(CommonGLPI $item, $tabnum = 1, $withtemplate = 0) {
      if ($item->getType() == __CLASS__) {
         switch ($tabnum) {
            case 1:
               $item->showFormAppConfig();
               break;
         }
      }
      return true;
   }

   function showFormAppConfig() {
      $readonly = '';
      $opt = [];
      if (!self::canWrite()) {
         $readonly = ' readonly="readonly" ';
         $opt = ['readonly' => 'readonly'];
      }
      $client_id = $this->get('client_id', '');
      $client_secret = $this->get('client_secret', '');

      echo '<div class="center" id="tabsbody">';
      echo '<form name="form" action="' . Toolbox::getItemTypeFormURL(__CLASS__) . '" method="post">';
      echo '<table class="tab_cadre_fixe">';
      echo '<tr><th colspan="4">' . __('Настройка приложения', 'wunderlist') . '</th></tr>';

      echo '<tr class="tab_bg_2">';
      echo '<td>' . __('Client Id', 'wunderlist') . '</td>';
      echo '<td><input type="text" name="client_id" size="60" value="' .  $client_id . '"' . $readonly . '></td>';
      echo '</tr>';

      echo '<tr class="tab_bg_2">';
      echo '<td>' . __('Client Secret', 'wunderlist') . '</td>';
      echo '<td><input type="text" name="client_secret" size="60" value="' .  $client_secret . '"' . $readonly . '></td>';
      echo '</tr>';

      echo '<tr class="tab_bg_2">';
      echo '<td colspan="4" class="center">';
      echo Html::submit(_sx('button', 'Save'), ['name' => 'update_app_config'] + $opt);
      echo '</td>';
      echo '</tr>';
      echo '</table>';
      Html::closeForm();
      echo '</div>';
   }

   function save($post) {
      if (self::canWrite()) {
         if (isset($post['update_app_config'])) {
            $this->set('client_id', $post['client_id']);
            $this->set('client_secret', $post['client_secret']);
         }
      }
   }

}
