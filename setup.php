<?php
/*
 -------------------------------------------------------------------------
 wunderlist plugin for GLPI
 Copyright (C) 2017 by the wunderlist Development Team.

 https://bitbucket.org/staltrans/wunderlist
 -------------------------------------------------------------------------

 LICENSE

 This file is part of wunderlist.

 wunderlist is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 wunderlist is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with wunderlist. If not, see <http://www.gnu.org/licenses/>.
 --------------------------------------------------------------------------
 */

define('PLUGIN_WUNDERLIST_VERSION', '0.3');

/**
 * Init hooks of the plugin.
 * REQUIRED
 *
 * @return void
 */
function plugin_init_wunderlist() {

   global $PLUGIN_HOOKS;

   Plugin::registerClass('PluginWunderlistAuth', ['addtabon' => 'Preference']);
   Plugin::registerClass('PluginWunderlistProfile', ['addtabon' => 'Profile']);

   $PLUGIN_HOOKS['menu_toadd']['wunderlist'] = ['plugins' => 'PluginWunderlistAuth'];

   $PLUGIN_HOOKS['csrf_compliant']['wunderlist'] = true;

   $PLUGIN_HOOKS['item_add']['wunderlist'] = [
      // CommonITILTask
      'TicketTask'         => ['PluginWunderlistHook', 'itemAddCommonITILTask'],
      'ProblemTask'        => ['PluginWunderlistHook', 'itemAddCommonITILTask'],
      'ChangeTask'         => ['PluginWunderlistHook', 'itemAddCommonITILTask'],
      // ProjectTask
      'ProjectTask'        => ['PluginWunderlistHook', 'itemAddProjectTask'],
      // Reminder 
      'Reminder'           => ['PluginWunderlistHook', 'itemAddReminder'],

   ];

   $PLUGIN_HOOKS['item_update']['wunderlist'] = [
      // CommonITILTask
      'TicketTask'         => ['PluginWunderlistHook', 'itemUpdateCommonITILTask'],
      'ProblemTask'        => ['PluginWunderlistHook', 'itemUpdateCommonITILTask'],
      'ChangeTask'         => ['PluginWunderlistHook', 'itemUpdateCommonITILTask'],
      // ProjectTask
      'ProjectTask'        => ['PluginWunderlistHook', 'itemUpdateProjectTask'],
      // Reminder 
      'Reminder'           => ['PluginWunderlistHook', 'itemUpdateReminder'],
   ];

   $PLUGIN_HOOKS['item_delete']['wunderlist'] = [
      // CommonITILTask
      'TicketTask'         => ['PluginWunderlistHook', 'itemDelCommonITILTask'],
      'ProblemTask'        => ['PluginWunderlistHook', 'itemDelCommonITILTask'],
      'ChangeTask'         => ['PluginWunderlistHook', 'itemDelCommonITILTask'],
      // ProjectTask
      'ProjectTask'        => ['PluginWunderlistHook', 'itemDelProjectTask'],
      // Reminder 
      'Reminder'           => ['PluginWunderlistHook', 'itemDelReminder'],
   ];

   $PLUGIN_HOOKS['item_purge']['wunderlist'] = $PLUGIN_HOOKS['item_delete']['wunderlist'];


}


/**
 * Get the name and the version of the plugin
 * REQUIRED
 *
 * @return array
 */
function plugin_version_wunderlist() {
   return [
      'name'           => 'wunderlist',
      'version'        => PLUGIN_WUNDERLIST_VERSION,
      'author'         => '<a href="https://bitbucket.org/staltrans/">StalTrans</a>',
      'license'        => 'GPLv3',
      'homepage'       => 'https://bitbucket.org/staltrans/glpi-wunderlist',
      'minGlpiVersion' => '9.2',
   ];
}

/**
 * Check pre-requisites before install
 * OPTIONNAL, but recommanded
 *
 * @return boolean
 */
function plugin_wunderlist_check_prerequisites() {
  $plugin = new Plugin();
   if (!$plugin->isInstalled('config')) {
      printf(__('This plugin requires %s', 'wunderlist'), '<a href="https://bitbucket.org/staltrans/glpi-config">Config GLPI plugin</a>');
      return false;
   }
   if (!$plugin->isActivated('config')) {
      printf(__('Please activate %s', 'wunderlist'), '<a href="https://bitbucket.org/staltrans/glpi-config">Config GLPI plugin</a>');
      return false;
   }
   if (!$plugin->isInstalled('libraries')) {
      printf(__('This plugin requires %s', 'wunderlist'), '<a href="https://bitbucket.org/staltrans/glpi-libraries">Libraries GLPI plugin</a>');
      return false;
   }
   if (!$plugin->isActivated('libraries')) {
      printf(__('Please activate %s', 'wunderlist'), '<a href="https://bitbucket.org/staltrans/glpi-libraries">Libraries GLPI plugin</a>');
      return false;
   }
   if(!class_exists('Wunderlist\Api')) {
      printf(__('Please install %s', 'wunderlist'), '<a href="' . PluginLibrariesList::getURL() . '">staltrans/wunderlist-rest-api</a>');
      return false;
   }

   // Strict version check (could be less strict, or could allow various version)
   if (version_compare(GLPI_VERSION, '9.2', 'lt')) {
      if (method_exists('Plugin', 'messageIncompatible')) {
         echo Plugin::messageIncompatible('core', '9.2');
      } else {
         echo "This plugin requires GLPI >= 9.2";
      }
      return false;
   }
   return true;
}

/**
 * Check configuration process
 *
 * @param boolean $verbose Whether to display message on failure. Defaults to false
 *
 * @return boolean
 */
function plugin_wunderlist_check_config($verbose = false) {
   if (true) { // Your configuration check
      return true;
   }

   if ($verbose) {
      _e('Installed / not configured', 'wunderlist');
   }
   return false;
}

function plugin_wunderlist_libraries_composer_help() {
   return [
      'title' => sprintf(__('Install %s', 'wunderlist'), 'staltrans/wunderlist-rest-api'),
      'content' => '[... how to install staltrans/wunderlist-rest-api ...]'
   ];
}
